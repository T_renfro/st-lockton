
## Setting Up Project

Install dependencies.
```
npm install
```

Serve locally.
After everything is set up, you will now have a working enviornment that will watch changes from your project, compile SASS to CSS, minify JS, and live reload. All commands are listed in the package.json file.
```
npm run serve
```






