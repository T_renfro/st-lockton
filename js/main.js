/**
* bulmajs - version 0.0.1 - 18-08-2016
* JavaScript Library for the Bulma CSS Framework.
* © 2016 Dominic Rico Gómez <hello@coderocket.me> 
*/

var BULMA;

if (typeof BULMA !== "object") {
  BULMA = {};
}

(function() {
  var hashCode;
  BULMA.hide = function(el) {
    var display;
    display = BULMA.isVisible(el);
    if (display) {
      el.style.display = 'none';
    }
  };
  BULMA.show = function(el) {
    var display;
    display = BULMA.isVisible(el);
    if (!display) {
      el.style.display = 'block';
    }
  };
  BULMA.toggle = function(el) {
    var display;
    display = BULMA.isVisible(el);
    if (!display) {
      el.style.display = 'block';
    } else {
      el.style.display = 'none';
    }
  };
  BULMA.getElements = function(name) {
    return document.querySelectorAll('[data-bulma="' + name + '"]');
  };
  BULMA.isVisible = function(el) {
    var display;
    if (window.getComputedStyle) {
      display = getComputedStyle(el, null).display;
    } else {
      display = el.currentStyle.display;
    }
    return display !== 'none';
  };
  BULMA.hasClass = function(el, className) {
    if (el.classList) {
      return el.classList.contains(className);
    } else {
      return new RegExp('\\b' + className + '\\b').test(el.className);
    }
  };
  BULMA.addClass = function(el, className) {
    if (el.classList) {
      return el.classList.add(className);
    } else if (!BULMA.hasClass(el, className)) {
      return el.className += ' ' + className;
    }
  };
  BULMA.removeClass = function(el, className) {
    if (el.classList) {
      return el.classList.remove(className);
    } else {
      return el.className = el.className.replace(new RegExp('\\b' + className + '\\b', 'g'), '');
    }
  };
  BULMA.parseOptions = function(el) {
    var j, len, option, options, opts;
    opts = {};
    options = el.getAttribute('data-options');
    options = (options || '').replace(/\s/g, '').split(';');
    for (j = 0, len = options.length; j < len; j++) {
      option = options[j];
      if (option) {
        option = option.split(':');
        opts[option[0]] = option[1];
      }
    }
    return opts;
  };
  BULMA.click = function(el, handler) {
    if (!el.eventListener) {
      el.eventListener = true;
      return el.addEventListener('click', handler);
    }
  };
  BULMA.unclick = function(el, handler) {
    if (el.eventListener) {
      el.eventListener = false;
      return el.removeEventListener('click', handler);
    }
  };
  if (document.readyState !== 'loading') {
    BULMA.isReady = true;
    return;
  } else if (document.addEventListener) {
    document.addEventListener('DOMContentLoaded', function() {
      BULMA.isReady = true;
    });
  } else {
    document.attachEvent('onreadystatechange', function() {
      if (document.readyState === 'complete') {
        BULMA.isReady = true;
      }
    });
  }
  return hashCode = function(str) {
    var hash, i, j, len, s;
    hash = 0;
    for (i = j = 0, len = str.length; j < len; i = ++j) {
      s = str[i];
      hash = ~~(((hash << 5) - hash) + str.charCodeAt(i));
    }
    return hash;
  };
})();
var i, j, len, len1, tab, tabs, target, targets;

BULMA.toggleTab = function(el) {
  var i, l, len, links;
  links = el.target.parentNode.parentNode;
  links = links.querySelectorAll('li');
  for (i = 0, len = links.length; i < len; i++) {
    l = links[i];
    BULMA.removeClass(l, 'is-active');
    BULMA.hide(document.querySelector(l.firstChild.getAttribute('data-tab')));
  }
  BULMA.addClass(el.target.parentNode, 'is-active');
  BULMA.show(document.querySelector(el.target.getAttribute('data-tab')));
};

if (!BULMA.isReady) {
  tabs = BULMA.getElements('tabs');
  if (tabs && tabs.length > 0) {
    for (i = 0, len = tabs.length; i < len; i++) {
      tab = tabs[i];
      targets = tab.querySelectorAll('[data-tab]');
      for (j = 0, len1 = targets.length; j < len1; j++) {
        target = targets[j];
        tab = document.querySelector(target.getAttribute('data-tab'));
        if (BULMA.hasClass(target.parentNode, 'is-active') === false) {
          BULMA.hide(tab);
        }
        BULMA.click(target, BULMA.toggleTab);
      }
    }
  }
}

document.getElementById("nav-toggle").addEventListener("click", toggleNav);
function toggleNav() {
  var nav = document.getElementById("nav-menu");
  var className = nav.getAttribute("class");
  if (className == "nav-right nav-menu") {
    nav.className = "nav-right nav-menu is-active";
  } else {
    nav.className = "nav-right nav-menu";
  }
}